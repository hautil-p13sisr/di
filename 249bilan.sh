#!/bin/bash

set -x

id ; date ; date -R

ip r

w

systemctl status --no-pager

last

tail /var/log/auth.log

tail /var/log/apache2/access.log

exit 0
