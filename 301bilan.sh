#!/bin/bash

set -x

id ; date ; date -R

nmcli

systemctl status --no-pager

journalctl --boot --no-pager

journalctl --boot=-4 --no-pager

journalctl --list-boots --no-pager

ls -rtlh /var/log
tail --lines=12 /var/log/auth.log

ls -rtlh /var/log/apache2
tail --lines=12 /var/log/apache2/access.log

ls -rtlh /var/log/apt

grep sudo /etc/group
grep adm /etc/group

ls -rtlh ~/public_html
stat ~/public_html

exit 0
