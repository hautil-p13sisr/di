# Terrain du Stage d'Observation 2023

 No | Commune | Raison sociale |
----|---------|----------------|
  1 |                   |                                            |
  2 |                   |                                            |
  3 |                   |                                            |
  4 |Paris (75008)      |		Hays Recruiement             |
  5 |Cergy-Pontoise     |Mairie de Cergy                             |
  6 |Paris              |CEFFIE                                      |
  7 | Cergy-Pontoise | Communauté d'agglomeration de Cergy-Pontoise 
  8 | Guyancourt        |WHPC SAS                                            |
  9 | Saint-Ouen-l'Aumône | SRT-Systems |
 10 |Poissy             |Service Reseau Infrastructure de Stellantis |
 11 |Nul part           |A la ramasse                                |
 12 |                   |A la ramasse                                |
 13 |Nul part           | A la ramasse                               |
